&emsp;&emsp;鉴于客户只能在离线环境安装和Windows Server 2008 R2系统上进行部署，经过笔者今天一天的尝试，终于成功探索出这条路，因此希望能分享给大家。

&emsp;&emsp;`本文将介绍如何在Windows系统（离线）中安装和使用Docker、Docker-compose。`

#### 预备工作：

1. 检查安装系统是否为Windows系统，以及该系统是32位还是64位，本文支持Windows Server 2008 R2, Windows7, Windows10;
2. 检查系统是否开启虚拟化，这可以在系统启动前的BIOS系统中的Advanced --> CPU --> Intel Virtual Technology，如果是disabled，则改成enabled；
3. 系统中是否安装有Vitrual Box, 如果有，该Vitrual Box用其他虚拟机软件代替，比如VMware，因为Docker在安装的时候也会安装Vitrual Box，这会导致冲突，可能原有的Vitrual Box会无法使用。

#### 安装Docker, Docker-Compose

&emsp;&emsp;本文支持Windows Server 2008 R2, Windows7, Windows10。

&emsp;&emsp;点击DockerToolbox-19.03.1.exe，进行安装，一直点击"Next"就可以完成安装，安装完后桌面上会多出三个图标，分别是：

- Docker Quickstart Terminal
- Kitematic Alpha
- Oracle VM VitrualBox

其中Docker Quickstart Terminal为Docker操作软件，Kitematic Alpha为Docker容器管理软件，Oracle VM VitrualBox为虚拟机。

&emsp;&emsp;一般，Docker的默认安装路径为"C://Program Files/Docker Toolbox"。我们需要把`boo2docker.iso文件`拷贝至"C://Users/Administator/.docker/machine/cache"，这样就完成了Docker安装。
点击"Docker Quickstart Terminal"， 可以使用输入'Docker version'查看Docker版本，本次版本为`19.03.1`。

&emsp;&emsp;这时候docker-compose也在后台安装好了，可以输入"docker-compose version"查看，本次docker-compose版本为`1.21.1`。如果在输入"docker-compose version"，提示如下错误：
"无法定位程序输入点ucrtbase.abort与动态链接库api-ms-win-crt-runtime-l1-1-0.dll上"，则需要安装Visual C++ Redistributable for Visual Studio 2015。我们选择安装vc_redist文件即可，32位的安装vc_redist.x86.ext,
64位系统的安装vc_redist.x64.exe即可。

&emsp;&emsp;这样就完成在Windows系统上Docker,Docker-Compose的安装。

&emsp;&emsp;如果需要测试Docker,可以加载hello-world-docker.tar.gz镜像；如果需要测试docker-compose,可以加载mysq.tar.gz镜像，用docker-compose up -d启动，该镜像为MySQL镜像。

### 使用Docker-Compose部署系统

&emsp;&emsp;点击`Docker Quickstart Terminal`软件，在这里能操作Docker和Docker-Compose,使用的方式和Linux环境下的Docker类似，支持Linux命令，比如cd,ls,也支持Docker 命令，
比如docker images, docker ps, docker-compose up -d。

&emsp;&emsp;离线环境下，一般会将镜像压缩成包，使用`docker save`命令；加载镜像，一般使用`docker load`命令。

&emsp;&emsp;用docker或者docker-compose启动容器后，可以使用Kitematic Alpha查看docker容器的运行情况以及服务的IP地址，这个很有用。

### 总结

&emsp;&emsp;时间有限，能力有限，尝试有限，如有不足，敬请谅解。该文档有待后续完善，作为一次客户现场的尝试，希望能对大家有用。