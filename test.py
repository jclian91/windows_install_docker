# -*- coding: utf-8 -*-
# author: Jclian91
# place: Pudong Shanghai
# time: 16:15
import os, re, json, traceback
import demjson

if __name__ == '__main__':
    a = {'a': [1, 2, 3],
         'b': 'bhh',
         'c': [[1,2], [3,4]]
        }
    # print(json.dumps(a, indent=2))
    print(demjson.encode(a, compactly=False, indent_limit=10))